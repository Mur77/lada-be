<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Описание полей таблицы
 * id       id
 * name     string  Имя
 * m_name   string  Отчество
 * s_name   string  Фамилия
 * phone    string  Телефон
 * address  string  Адрес
 * comment  string  Комментарий
 */

class People extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'm_name', 's_name', 'phone', 'address', 'comment'];

    protected $guarded = [];


    /** 
     * RequestValidationRules
     * Правида валидации для HTTP Request
     */
    public static function RequestValidationRules()
    {
        return [
            'name' => 'required|max:20',
            'm_name' => 'max:20',
            's_name' => 'required|max:20',
            'phone' => 'required|size:13',
            'address' => 'required|max:255',
            'comment' => 'max:255',
        ];
    }

}

