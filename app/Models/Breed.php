<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Описание полей таблицы
 * id           id
 * name_en      порода на английском
 * name_ru      порода на русском
 * description  описание
 */

class Breed extends Model
{
    use HasFactory;

    protected $fillable = ['name_en', 'name_ru', 'description'];

    protected $guarded = [];


    /** 
     * RequestValidationRules
     * Правида валидации для HTTP Request
     */
    public static function RequestValidationRules()
    {
        return [
            'name_en' => 'required|max:20',
            'name_ru' => 'required|max:20',
            'description' => 'nullable|max:255',
        ];
    }

}

