<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Описание полей таблицы
 * id       id
 * name     string  Имя или название организации
 * phone    string  Телефон
 * address  string  Адрес
 * comment  string  Комментарий
 */

class Counterparty extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'phone', 'address', 'comment'];

    protected $guarded = [];

    protected $table = 'counterparties';

    /** 
     * RequestValidationRules
     * Правида валидации для HTTP Request
     */
    public static function RequestValidationRules()
    {
        return [
            'name' => 'required|max:20',
            'phone' => 'required|size:13',
            'address' => 'max:100',
            'comment' => 'max:255',
        ];
    }

}

