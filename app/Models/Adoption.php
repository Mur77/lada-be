<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Описание полей таблицы
 * id           id
 * date         date    дата передачи в добрые руки
 * people_id    integer кому передано животное
 * comment      string  описание
 */

class Adoption extends Model
{
    use HasFactory;

    protected $fillable = ['date', 'people_id', 'comment'];

    protected $guarded = [];


    /**
     * Get the People associated with the Adoption.
     */
    public function people()
    {
        return $this->hasOne(People::class, 'id', 'people_id');
    }

    /** 
     * RequestValidationRules
     * Правида валидации для HTTP Request
     */
    public static function RequestValidationRules()
    {
        return [
            'date' => 'required',
            'people_id' => 'required',
            'comment' => 'nullable|max:255',
        ];
    }

}

