<?php

namespace App\Models;

use Countable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Описание полей таблицы
 * id               id
 * date             date    дата транзакции
 * operation_id     integer id вида операции (приход / расход)
 * counterparty_id  integer id контрагента
 * item_id          integer id статьи расходов (доходов)
 * amount           float   сумма
 * comment          string  комментарий
 */

class File extends Model
{
    use HasFactory;

    protected $fillable = ['date', 'operation_id', 'counterparty_id', 'item_id', 'amount', 'comment'];

    protected $guarded = [];


    /**
     * Get the Gender associated with the Animal.
     */
    public function operation()
    {
        return $this->hasOne(Operation::class, 'id', 'operation_id');
    }

    /**
     * Get the Color associated with the Animal.
     */
    public function counterparty()
    {
        return $this->hasOne(Counterparty::class, 'id', 'counterparty_id');
    }


    /**
     * Get the Breed associated with the Animal.
     */
    public function item()
    {
        return $this->hasOne(Item::class, 'id', 'item_id');
    }

    /**
     * Get the Files associated with the Animal.
     */
    public function files()
    {
        return $this->hasMany(File::class, 'transaction_id', 'id');
    }


    /** 
     * RequestValidationRules
     * Правида валидации для HTTP Request
     */
    public static function RequestValidationRules()
    {
        return [
            'date' => 'required',
            'operation_id' => 'required',
            'counterparty_id' => 'required',
            'item_id' => 'required',
            'amount' => 'required',
            'comment' => 'nullable|max:255',
        ];
    }

}

