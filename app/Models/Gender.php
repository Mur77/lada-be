<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Описание полей таблицы
 * id           id
 * name_en      string  пол на английском
 * name_ru      string  пол на русском
 * description  string  описание
 */

class Gender extends Model
{
    use HasFactory;

    protected $fillable = ['name_en', 'name_ru', 'description'];

    protected $guarded = [];


    /** 
     * RequestValidationRules
     * Правида валидации для HTTP Request
     */
    public static function RequestValidationRules()
    {
        return [
            'name_en' => 'required|max:6',
            'name_ru' => 'required|max:7',
            'description' => 'nullable|max:255',
        ];
    }

}

