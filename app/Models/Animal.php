<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Описание полей таблицы
 * id           id
 * nickname_ru  string  кличка на русском
 * nickname_en  string  кличка на английском
 * kind_id      integer вид животного
 * gender_id    integer пол
 * description  string  описание
 * birthday     date    день рождения
 * color_id     integer цвет
 * breed_id     integer порода
 * enter_date   date    дата поступления
 * death_date   date    дата смерти
 * week_animal  integer животное недели
 * can_adopt    boolean возможно ли отдать в добрые руки
 * adoption_id  integer ссылка на запись в таблице adoption если животное отдано в добрые руки
 * comment      string  комментарий
 */

class Animal extends Model
{
    use HasFactory;

    protected $fillable = ['nickname_ru', 'nickname_en', 'kind_id', 'gender_id', 'description', 'birthday', 'color_id', 'breed_id',
        'enter_date', 'death_date', 'week_animal', 'can_adopt', 'adoption_id', 'comment'];

    protected $guarded = [];


    /**
     * Get the Kind associated with the Animal.
     */
    public function kind()
    {
        return $this->hasOne(Kind::class, 'id', 'kind_id');
    }

    /**
     * Get the Gender associated with the Animal.
     */
    public function gender()
    {
        return $this->hasOne(Gender::class, 'id', 'gender_id');
    }

    /**
     * Get the Color associated with the Animal.
     */
    public function color()
    {
        return $this->hasOne(Color::class, 'id', 'color_id');
    }


    /**
     * Get the Breed associated with the Animal.
     */
    public function breed()
    {
        return $this->hasOne(Breed::class, 'id', 'breed_id');
    }


    /**
     * Get the Adoption associated with the Animal.
     */
    public function adoption()
    {
        return $this->hasOne(Adoption::class, 'id', 'adoption_id');
    }

    /**
     * Get the Files associated with the Animal.
     */
    public function files()
    {
        return $this->hasMany(File::class, 'animal_id', 'id');
    }


    /** 
     * RequestValidationRules
     * Правида валидации для HTTP Request
     */
    public static function RequestValidationRules()
    {
        return [
            'nickname_ru' => 'required|max:20',
            'nickname_en' => 'required|max:20',
            'kind_id' => 'required',
            'gender_id' => 'required',
            'description' => 'required|max:255',
            'birthday' => 'nullable',
            'color_id' => 'required',
            'breed_id' => 'nullable',
            'enter_date' => 'required',
            'death_date' => 'required',
            'week_animal' => 'max:1',
            'can_adopt' => 'required:max:1',
            'adoption_id' => 'nullable',
            'comment' => 'nullable|max:255',
        ];
    }

}

