<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Описание полей таблицы
 * id               id
 * animal_id        integer животное, для которого файл
 * user_id          integer пользователь, для которого файл
 * transaction_id   integer транзакция, для которой файл
 * type_id          integer тип файла (фото, чек, ...)
 * description      string  описание
 * show_on_slider   boolean показывать на слайдере на главной (для картинок)
 * path             string  путь к файлу
 */

class File extends Model
{
    use HasFactory;

    protected $fillable = ['animal_id', 'user_id', 'transaction_id', 'type_id', 'description',
        'show_on_slider', 'path'];

    protected $guarded = [];


    /**
     * Get the Doctype of the File.
     */
    public function doctype()
    {
        return $this->hasOne(Doctype::class, 'id', 'type_id');
    }


    /** 
     * RequestValidationRules
     * Правида валидации для HTTP Request
     */
    public static function RequestValidationRules()
    {
        return [
            'animal_id' => 'nullable',
            'user_id' => 'nullable',
            'transaction_id' => 'nullable',
            'type_id' => 'required',
            'description' => 'nullable|max:255',
            'show_on_slider' => 'required',
            'path' => 'required',
        ];
    }

}

