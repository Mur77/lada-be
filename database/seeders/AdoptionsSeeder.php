<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdoptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('adoptions')->insert([
            'date' => '15.04.2021',
            'people_id' => '0',
            'comment' => '',
        ]);

        DB::table('adoptions')->insert([
            'date' => '25.10.2021',
            'people_id' => '1',
            'comment' => '',
        ]);
    }
}
