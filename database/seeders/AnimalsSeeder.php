<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AnimalsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('animals')->insert([
            'nickname_ru' => 'Кася',
            'nickname_en' => 'Kasya',
            'kind_id' => 0,
            'gender_id' => 1,
            'description' => '',
            'birthday' => '12.01.2021',
            'color_id' => '0',
            'breed_id' => '0', 
            'enter_date' => '20.01.2021',
            'death_date' => NULL,
            'week_animal' => 1,
            'can_adopt' => '1',
            'adoption_id' => NULL,
            'comment' => '',
        ]);

        DB::table('animals')->insert([
            'nickname_ru' => 'Бобик',
            'nickname_en' => 'Bobik',
            'kind_id' => 1,
            'gender_id' => 0,
            'description' => '',
            'birthday' => '12.01.2020',
            'color_id' => '1',
            'breed_id' => '0', 
            'enter_date' => '20.01.2020',
            'death_date' => NULL, 
            'week_animal' => 1,
            'can_adopt' => '1',
            'adoption_id' => NULL,
            'comment' => '',
        ]);
    }
}
