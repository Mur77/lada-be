<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class BreedsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('breeds')->insert([
            'name_en' => 'British',
            'name_ru' => 'Британец',
            'description' => '',
        ]);

        DB::table('kinds')->insert([
            'name_en' => 'Jack Russel Terrier',
            'name_ru' => 'Джек Рассел Терьер',
            'description' => '',
        ]);

        DB::table('breeds')->insert([
            'name_en' => 'Sphynx',
            'name_ru' => 'Сфинкс',
            'description' => '',
        ]);

        DB::table('kinds')->insert([
            'name_en' => 'Bakhmut Guard',
            'name_ru' => 'Бахмутская Сторожевая',
            'description' => '',
        ]);
    }
}
