<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('operations')->insert([
            'name_en' => 'Donations',
            'name_ru' => 'Благотворительные взносы',
            'description' => '',
        ]);

        DB::table('operations')->insert([
            'name_en' => 'Meat',
            'name_ru' => 'Мясо',
            'description' => '',
        ]);

        DB::table('operations')->insert([
            'name_en' => 'Dry feed',
            'name_ru' => 'Сухой корм',
            'description' => '',
        ]);

        DB::table('operations')->insert([
            'name_en' => 'Medicines',
            'name_ru' => 'Мед. препараты',
            'description' => '',
        ]);

        DB::table('operations')->insert([
            'name_en' => 'Veterinary services',
            'name_ru' => 'Ветеринарные услуги',
            'description' => '',
        ]);
    }
}
