<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DoctypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('doctypes')->insert([
            'name_en' => 'Picture',
            'name_ru' => 'Фото',
            'description' => '',
        ]);

        DB::table('doctypes')->insert([
            'name_en' => 'Passport',
            'name_ru' => 'Паспорт',
            'description' => '',
        ]);

        DB::table('doctypes')->insert([
            'name_en' => 'Receipt',
            'name_ru' => 'Чек',
            'description' => '',
        ]);
    }
}
