<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ColorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('colors')->insert([
            'name_en' => 'Black',
            'name_ru' => 'Черный',
            'description' => '',
        ]);

        DB::table('colors')->insert([
            'name_en' => 'Pink',
            'name_ru' => 'Розовый',
            'description' => '',
        ]);

        DB::table('colors')->insert([
            'name_en' => 'Yellow',
            'name_ru' => 'Желтый',
            'description' => '',
        ]);
    }
}
