<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TransactionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transactions')->insert([
            'date' => '14.09.2021',
            'operation_id' => 0,
            'counterparty_id' => 0,
            'item_id' => 0,
            'amount' => '200.00',
            'comment' => '',
        ]);

        DB::table('transactions')->insert([
            'date' => '15.09.2021',
            'operation_id' => 0,
            'counterparty_id' => 0,
            'item_id' => 0,
            'amount' => '300.00',
            'comment' => '',
        ]);

        DB::table('transactions')->insert([
            'date' => '16.09.2021',
            'operation_id' => 1,
            'counterparty_id' => 1,
            'item_id' => 3,
            'amount' => '400.00',
            'comment' => '',
        ]);
    }
}
