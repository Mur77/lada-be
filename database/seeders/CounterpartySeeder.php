<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CounterpartySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('counterparties')->insert([
            'name' => 'ООО "Заречье"',
            'phone' => '+380983575383',
            'address' => 'г.Бахмут, ул.Заречная 12',
            'comment' => '',
        ]);

        DB::table('counterparties')->insert([
            'name' => 'Владимир',
            'phone' => '+380973847261',
            'address' => 'с.Закотное, ул.Ленина 14',
            'comment' => '',
        ]);
    }
}
