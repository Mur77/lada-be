<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PeoplesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('peoples')->insert([
            'name' => 'Валентина',
            'm_name' => 'Михайловна',
            's_name' => 'Кузнецова',
            'phone' => '+380989387583',
            'address' => 'г.Бахмут, ул.Независимости 71, кв.5',
            'comment' => '',
        ]);

        DB::table('peoples')->insert([
            'name' => 'Владимир',
            'm_name' => 'Владимирович',
            's_name' => 'Мокляков',
            'phone' => '+380979498765',
            'address' => 'г.Бахмут, ул.Чайковского 34, кв.40',
            'comment' => '',
        ]);
    }
}
