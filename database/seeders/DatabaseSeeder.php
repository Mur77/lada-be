<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            KindsSeeder::class,
            ColorsSeeder::class,
            BreedsSeeder::class,
            GendersSeeder::class,
            PeoplesSeeder::class,
            AdoptionsSeeder::class,
            AnimalsSeeder::class,
            CounterpartySeeder::class,
            OperationsSeeder::class,
            ItemsSeeder::class,
            TransactionsSeeder::class,
            DoctypesSeeder::class,
        ]);
    }
}
