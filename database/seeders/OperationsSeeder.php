<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OperationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('operations')->insert([
            'name_en' => 'Income',
            'name_ru' => 'Доход',
            'description' => '',
        ]);

        DB::table('kinds')->insert([
            'name_en' => 'Outcome',
            'name_ru' => 'Расход',
            'description' => '',
        ]);
    }
}
