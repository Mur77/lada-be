<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class KindsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kinds')->insert([
            'name_en' => 'Cat',
            'name_ru' => 'Кот',
            'description' => '',
        ]);

        DB::table('kinds')->insert([
            'name_en' => 'Dog',
            'name_ru' => 'Собака',
            'description' => '',
        ]);
    }
}
