<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class GendersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genders')->insert([
            'name_en' => 'Male',
            'name_ru' => 'Мужской',
            'description' => '',
        ]);

        DB::table('genders')->insert([
            'name_en' => 'Female',
            'name_ru' => 'Женский',
            'description' => '',
        ]);
    }
}
