<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnimalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animals', function (Blueprint $table) {
            $table->id();
            $table->string('nickname_en');
            $table->string('nickname_ru');
            $table->integer('kind_id');
            $table->integer('gender_id');
            $table->string('description');
            $table->date('birthday')->nullable();
            $table->integer('color_id');
            $table->integer('breed_id')->nullable();
            $table->date('enter_date');
            $table->date('death_date')->nullable();
            $table->integer('week_animal')->default(0);
            $table->boolean('can_adopt')->default(true);
            $table->integer('adoption_id')->nullable();
            $table->string('comment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animals');
    }
}
